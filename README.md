﻿Bloody Playground
===========

2D multiplayer shooter game in Java. 

About
-----

This project was created to:
 - learn more about game development
 - entertain myself
 - entertain others

Author
----------
Marcel Fiala
macek.fiala@gmail.com

Special Thanks
---------------
- testing: Marcel Fiala, Jan Tichy, Tomas Valda, David Micka
- inspiration, gamedev tutorials: Lukáš "Polarkac" Pohlreich

Changelog (from a certain point)
----------
### 11-11-2016 ###
- BUGFIX: Collision Handling for player works in 2nd and following games.
- BUGFIX: Shooting works in 2nd and following games.
- BUGFIX: In log, names of weapons and power-ups are specified (no "null" anymore)
- GAMEPLAY: PLayer 3 controls moved from numeric keyboard to arrows and space.